<?php

namespace Constant;

/**
 * Class ErrorMessage
 */
class ErrorMessage
{

    public const NOT_ENOUGH_CAPACITY = 'Not enough capacity, all of the items';
    public const NOT_IMPLEMENTED = 'This basic version can not able that';
}