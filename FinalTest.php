<?php

use Enum\BrandEnum;
use PHPUnit\Framework\TestCase;

/**
 * Class FinalTest
 */
class FinalTest extends TestCase
{

    /**
     * @var StoreController
     */
    private StoreController $storeController;

    /**
     * @var array|Store
     */
    private Store|array $stores = array();

    /**
     * @var Product|array
     */
    private Product|array $products = array();

    /**
     * @param int $numberOfProducts
     *
     * @return void
     * @throws Exception
     */
    private function addStores(int $numberOfProducts): void
    {

        $storeController = new StoreController();
        $this->stores[] = $storeController->addStore('TinyStore', 'Home 1', 2);
        $this->stores[] = $storeController->addStore('MiddleStore', 'Home 2', 5);
        $this->stores[] = $storeController->addStore('BigStore', 'Home 3', 8);
        $this->stores[] = $storeController->addStore('HugeStore', 'Home 4', 15);
        $this->addProducts($numberOfProducts);
        $this->storeController = $storeController;
    }

    /**
     * @param int $numberOfProducts
     *
     * @return void
     * @throws Exception
     */
    private function addProducts(int $numberOfProducts): void
    {

        $productController = new ProductController();
        $brand = (new Brand('Phillips', BrandEnum::GOOD->value));

        for ($i = 0; $i < $numberOfProducts; $i++) {
            $serialNumber = (string)random_int(10, 1000);
            $productName = uniqid('pn', true);
            $price = random_int(10, 10000) / 10;
            $this->products[] = $productController->addProduct($serialNumber, $brand, $productName, $price);
        }
    }

    /**
     * @throws Exception
     */
    public function testAddProducts(): void
    {
        $this->addStores(10);
        $this->storeController->addProductsToStores($this->stores, $this->products);
        $this->assertSame($this->stores[0]->getCapacity(), $this->stores[0]->getQuantity());
        $this->assertSame($this->stores[1]->getCapacity(), $this->stores[1]->getQuantity());
        $this->assertSame(3, $this->stores[2]->getQuantity());

        $storesItems = $this->storeController->showItemsByStores($this->stores);
        print_r($storesItems);
    }

    /**
     * @return void
     * @throws Exception
     */
    public function testAddTooMuchProducts(): void
    {
        $this->addStores(35);
        $notStoredProducts = $this->storeController->addProductsToStores($this->stores, $this->products);
        $this->assertNotEmpty($notStoredProducts, 'Error, all product can be stored');

        $storesItems = $this->storeController->showItemsByStores($this->stores);
        print_r($storesItems);
        echo 'Not stored products:';
        print_r($notStoredProducts);
    }

    /**
     * @return void
     * @throws Exception
     */
    public function testListProducts(): void
    {
        $this->addStores(4);
        $this->storeController->addProductsToStores($this->stores, $this->products);
        $this->assertSame($this->stores[0]->getCapacity(), $this->stores[0]->getQuantity());
        $productsOfStores = $this->storeController->searchItems($this->stores, $this->products);
        print_r($productsOfStores);
    }
}