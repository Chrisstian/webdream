<?php

use Enum\BrandEnum;
use Exception\CapacityException;
use PHPUnit\Framework\TestCase;

/**
 * Class StoreControllerTest
 */
class StoreControllerTest extends TestCase
{

    /**
     * @var array|Product
     */
    private Product|array $products = array();

    /**
     * @return void
     */
    protected function setUp(): void
    {
        $productController = new ProductController();
        $brand = (new Brand('Phillips', BrandEnum::WEAK->value));

        $this->products[] = $productController->addProduct('1234', $brand, 'First product', 10.5);
        $this->products[] = $productController->addProduct('5678', $brand, 'Second product', 11.0);
        $this->products[] = $productController->addProduct('999C', $brand, 'Third product', 131.0);
        $this->products[] = $productController->addProduct('44', $brand, 'Fourth product', 14.5);
        $this->products[] = $productController->addProduct('5555', $brand, 'Fifth product', 15.0);
    }

    /**
     * @return void
     * @throws CapacityException
     */
    public function testremoveProductsFromStoresController(): void
    {
        $storeController = new StoreController();
        $storeA = $storeController->addStore('StoreA', 'Home', 4);
        $storeB = $storeController->addStore('StoreB', 'Home', 5);
        $removableProducts = $this->products;
        unset($removableProducts[0], $removableProducts[2], $removableProducts[4]);

        $storeA->addProduct(array_shift($this->products));
        $storeA->addProduct(array_shift($this->products));
        $storeA->addProduct(array_shift($this->products));
        $storeB->addProduct(array_shift($this->products));
        $storeB->addProduct(array_shift($this->products));

        $notRemovedIds = $storeController->removeProductsFromStores([
            $storeA,
            $storeB
        ], $removableProducts);
        $this->assertCount(0, $notRemovedIds);
        $this->assertSame(2, $storeA->getQuantity());
        $this->assertSame(1, $storeB->getQuantity());
    }

    /**
     * @return void
     * @throws CapacityException
     */
    public function testaddProductToStoreController(): void
    {
        $storeController = new StoreController();
        $store = $storeController->addStore('Store', 'Home', 1);
        $storeController->addProductToStore($store, array_shift($this->products));
        $this->assertSame(1, $store->getQuantity());
        $this->expectException(CapacityException::class);
        $storeController->addProductToStore($store, array_shift($this->products));
    }

    /**
     * @return void
     * @throws CapacityException
     */
    public function testaddProductsToStoreController(): void
    {
        $storeController = new StoreController();
        $store = $storeController->addStore('Store', 'Home', 5);
        $storeController->addProductsToStore($store, array_slice($this->products, 1, 3));
        $this->assertSame(3, $store->getQuantity());
    }

    /**
     * @return void
     */
    public function testaddProductsToStoresController(): void
    {
        $storeController = new StoreController();
        $storeA = $storeController->addStore('StoreA', 'Home', 1);
        $storeB = $storeController->addStore('StoreB', 'Home', 2);
        $notStoredProducts = $storeController->addProductsToStores([
            $storeA,
            $storeB
        ], $this->products);
        $this->assertCount(2, $notStoredProducts);
        $this->assertSame(1, $storeA->getQuantity());
        $this->assertSame(2, $storeB->getQuantity());
    }
}