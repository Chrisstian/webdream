<?php

use Enum\BrandEnum;
use PHPUnit\Framework\TestCase;

/**
 * ProductControllerTest
 */
class ProductControllerTest extends TestCase
{

    /**
     * @return void
     */
    public function testAddProduct(): void
    {
        $controller = new ProductController();
        $brand = (new Brand('Phillips', BrandEnum::WEAK->value));

        $productA = $controller->addProduct('1234', $brand, 'First product', 10.5);
        $productB = $controller->addProduct('1235', $brand, 'Second product', 11.0);
        $this->assertSame(1, ($productB->getId() - $productA->getId()));
    }

    /**
     * @return void
     */
    public function testSetPrice(): void
    {
        $controller = new ProductController();
        $brand = (new Brand('Phillips', BrandEnum::WEAK->value));

        $product = $controller->addProduct('1234', $brand, 'First product', 10.5);
        $product->setPrice(10.5);
        $this->assertSame(10.5, $product->getPrice());
    }
}
