<?php

/**
 * Class ProductController
 */
class ProductController
{
    /**
     * @param string $serialNumber
     * @param Brand  $brand
     * @param string $name
     * @param float  $price
     *
     * @return Product
     */
    public function addProduct(string $serialNumber,
                               Brand  $brand,
                               string $name,
                               float  $price): Product
    {
        $product = new Product($serialNumber, $name, $price);
        $product->addBrand($brand);
        return $product;
    }

    /**
     * @param Product $product
     * @param float   $price
     *
     * @return void
     */
    public function modifyPrice(Product $product,
                                float   $price): void
    {
        $product->setPrice($price);
    }
}