<?php

use Constant\ErrorMessage;
use Exception\NotImplementedException;

/**
 * Class BrandController
 */
class BrandController
{

    /**
     * @param string $name
     * @param int    $quality
     *
     * @return Brand|string
     */
    public function addBrand(string $name,
                             int    $quality): Brand|string
    {
        try {
            return new Brand($name, $quality);
        } catch (Exception $exception) {
            return 'Error: ' . $exception->getMessage();
        }
    }

    /**
     * @param Brand $brand
     * @param int   $quality
     *
     * @return Brand|string
     */
    public function modifyQuality(Brand $brand,
                                  int   $quality): Brand|string
    {
        try {
            $brand->setQuality($quality);
            return $brand;
        } catch (Exception $exception) {
            return 'Error: ' . $exception->getMessage();
        }
    }

    /**
     * @param Brand $brand
     *
     * @return mixed
     * @throws NotImplementedException
     */
    public function deleteBrand(Brand $brand): mixed
    {
        throw new NotImplementedException(ErrorMessage::NOT_IMPLEMENTED);
    }
}