<?php

use Exception\CapacityException;

/**
 * Class StoreController
 */
class StoreController
{

    /**
     * @param string $name
     * @param string $address
     * @param int    $capacity
     *
     * @return Store
     */
    public function addStore(string $name,
                             string $address,
                             int    $capacity): Store
    {
        return new Store($name, $address, $capacity);
    }

    /**
     * @param Store $store
     *
     * @return array
     */
    public function showItemsByStore(Store $store): array
    {
        return $store->getProducts();
    }

    /**
     * @param array $stores
     *
     * @return array
     */
    public function showItemsByStores(array $stores): array
    {
        $result = array();
        foreach ($stores as $key => $store) {
            $result[$key]['storeId'] = $store->getId();
            $result[$key]['storeName'] = $store->getName();
            $result[$key]['products'] = $store->getProducts();
        }

        return $result;
    }

    /**
     * @param array $stores
     * @param array $products
     *
     * @return array
     */
    public function searchItems(array $stores,
                                array $products): array
    {
        $result = array();

        foreach ($products as $product) {
            $find = false;
            foreach ($stores as $store) {
                if ($store->containProduct($product->getId())) {
                    $result[$store->getName()][] = $product;
                    $find = true;
                    break;
                }
            }
            if ($find === false) {
                $result['notFindProducts'][] = $product;
            }
        }
        return $result;
    }

    /**
     * @param Store   $store
     * @param Product $product
     *
     * @return void
     * @throws CapacityException
     */
    public function addProductToStore(Store   $store,
                                      Product $product): void
    {
        $store->addProduct($product);
    }

    /**
     * @param Store $store
     * @param array $products
     *
     * @return void
     * @throws CapacityException
     */
    public function addProductsToStore(Store $store,
                                       array $products): void
    {
        $store->addProducts($products);
    }

    /**
     * @param array $stores
     * @param array $products
     *
     * @return array
     */
    public function addProductsToStores(array $stores,
                                        array $products): array
    {
        foreach ($stores as $store) {
            $vacancies = $store->getVacancies();
            if ($vacancies >= count($products)) {
                $store->addProducts($products);
                $products = array_slice($products, $vacancies);
                break;
            } else {
                $store->addProducts(array_slice($products, 0, $vacancies));
                $products = array_slice($products, $vacancies);
            }
        }
        return $products;
    }


    /**
     * @param Store $store
     * @param array $products
     *
     * @return array
     */
    public function removeProductsFromStore(Store $store,
                                            array $products): array
    {

        $productIds = array_map(function (Product $product) {
            return $product->getId();
        }, $products);
        return $store->removeProducts($productIds);
    }

    /**
     * @param array $stores
     * @param array $products
     *
     * @return array
     */
    public function removeProductsFromStores(array $stores,
                                             array $products): array
    {

        $productIds = array_map(function (Product $product) {
            return $product->getId();
        }, $products);

        foreach ($stores as $store) {
            $productIds = $store->removeProducts($productIds);
        }
        return $productIds;
    }
}