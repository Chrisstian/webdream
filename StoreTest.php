<?php

use Constant\ErrorMessage;
use Enum\BrandEnum;
use Exception\CapacityException;
use PHPUnit\Framework\TestCase;

/**
 * Class StoreTest
 */
class StoreTest extends TestCase
{

    /**
     * @var array|Product
     */
    private Product|array $products = array();

    /**
     * @return void
     */
    private function inicializationProducts(): void
    {
        $productController = new ProductController();
        $brand = (new Brand('Phillips', BrandEnum::WEAK->value));

        $this->products[] = $productController->addProduct('1234', $brand, 'First product', 10.5);
        $this->products[] = $productController->addProduct('5678', $brand, 'Second product', 11.0);
        $this->products[] = $productController->addProduct('999C', $brand, 'Third product', 131.0);
        $this->products[] = $productController->addProduct('44', $brand, 'Fourth product', 14.5);
        $this->products[] = $productController->addProduct('5555', $brand, 'Fifth product', 15.0);
    }

    /**
     * @throws CapacityException
     */
    public function testNotEnoughCapacity(): void
    {
        $this->inicializationProducts();
        $storeController = new StoreController();
        $store = $storeController->addStore('TinyStore', 'Home', 2);

        $store->addProduct(array_shift($this->products));
        $store->addProduct(array_shift($this->products));

        //3rd product has not enoug space for tinystore
        $this->expectExceptionMessage(ErrorMessage::NOT_ENOUGH_CAPACITY);
        $store->addProduct(array_shift($this->products));
    }

    /**
     * @throws CapacityException
     */
    public function testAddProductsWithEnoughCapacity(): void
    {
        $this->inicializationProducts();
        $storeController = new StoreController();
        $store = $storeController->addStore('TinyStore', 'Home', 5);
        $store->addProducts($this->products);
        $this->assertSame(5, $store->getQuantity());
    }

    /**
     * @throws CapacityException
     */
    public function testAddProductsNotEnoughCapacity(): void
    {
        $this->inicializationProducts();
        $productIds = array();
        $storeController = new StoreController();
        $store = $storeController->addStore('TinyStore', 'Home', 2);
        foreach ($this->products as $value) {
            $productIds[] = $value->getId();
        }
        $this->expectException(CapacityException::class);
        $store->addProducts($productIds);
        $this->assertSame(0, $store->getQuantity());
    }

    public function testremoveProductsFromStore(): void
    {
        $this->inicializationProducts();
        $storeController = new StoreController();
        $store = $storeController->addStore('TinyStore', 'Home', 4);
        $store->addProduct(array_shift($this->products));
        $store->addProduct(array_shift($this->products));
        $store->addProduct(array_shift($this->products));
        $notRemovedIds = $store->removeProducts([
            1,
            3,
            5
        ]);
        $this->assertCount(3, $notRemovedIds);
        $this->assertSame(3, $store->getQuantity());
    }

    public function testremoveProductsFromStoreController(): void
    {
        $this->inicializationProducts();
        $storeController = new StoreController();
        $store = $storeController->addStore('TinyStore', 'Home', 5);
        $removableProducts = $this->products;
        array_shift($removableProducts);
        $store->addProduct(array_shift($this->products));
        $store->addProduct(array_shift($this->products));
        $store->addProduct(array_shift($this->products));
        $store->addProduct(array_shift($this->products));
        $store->addProduct(array_shift($this->products));
        $notRemovedIds = $storeController->removeProductsFromStore($store, $removableProducts);
        $this->assertCount(0, $notRemovedIds);
        $this->assertSame(1, $store->getQuantity());
    }
}
