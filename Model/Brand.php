<?php

/**
 * Class Brand
 */
class Brand
{

    /**
     * @var int
     */
    private static int $brandId = 1;

    /**
     * @var int
     */
    private int $id;

    /**
     * @var string
     */
    private string $name;

    /**
     * @var int
     */
    private int $quality;

    /**
     * @param string $name
     * @param int    $quality
     */
    public function __construct(string $name,
                                int    $quality)
    {
        $this->id = self::$brandId++;
        $this->name = $name;
        $this->quality = $quality;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getQuality(): int
    {
        return $this->quality;
    }

    /**
     * @param int $quality
     */
    public function setQuality(int $quality): void
    {
        $this->quality = $quality;
    }
}