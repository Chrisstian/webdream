<?php

/**
 * Class Product
 */
class Product
{

    /**
     * @var int
     */
    private static int $productId = 1;

    /**
     * @var int
     */
    private int $id;

    /**
     * @var string
     */
    private string $serialNumber;

    /**
     * @var Brand
     */
    private Brand $brand;

    /**
     * @var string
     */
    private string $name;

    /**
     * @var float
     */
    private float $price;

    /**
     * @param string $serialNumber
     * @param string $name
     * @param float  $price
     */
    public function __construct(string $serialNumber,
                                string $name,
                                float  $price)
    {
        $this->id = self::$productId++;
        $this->serialNumber = $serialNumber;
        $this->name = $name;
        $this->price = $price;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getSerialNumber(): string
    {
        return $this->serialNumber;
    }

    /**
     * @param string $serialNumber
     */
    public function setSerialNumber(string $serialNumber): void
    {
        $this->serialNumber = $serialNumber;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice(float $price): void
    {
        $this->price = $price;
    }

    /**
     * @param Brand $brand
     *
     * @return void
     */
    public function addBrand(Brand $brand): void
    {
        $this->brand = $brand;
    }
}