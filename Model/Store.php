<?php

use Constant\ErrorMessage;
use Exception\CapacityException;

/**
 * Class Store
 */
class Store
{

    /**
     * @var int
     */
    private static int $storeId = 1;

    /**
     * @var int
     */
    private int $id;

    /**
     * @var array
     */
    private array $products = array();

    /**
     * @var string
     */
    private string $name;

    /**
     * @var string
     */
    private string $address;

    /**
     * @var int
     */
    private int $capacity;

    /**
     * @param string $name
     * @param string $address
     * @param int    $capacity
     */
    public function __construct(string $name,
                                string $address,
                                int    $capacity)
    {
        $this->id = self::$storeId++;
        $this->name = $name;
        $this->address = $address;
        $this->capacity = $capacity;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return array
     */
    public function getProducts(): array
    {
        return $this->products;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getAddress(): string
    {
        return $this->address;
    }

    /**
     * @param string $address
     *
     * @return void
     */
    public function setAddress(string $address): void
    {
        $this->address = $address;
    }

    /**
     * @return int
     */
    public function getCapacity(): int
    {
        return $this->capacity;
    }

    /**
     * @param int $capacity
     *
     * @return void
     */
    public function setCapacity(int $capacity): void
    {
        $this->capacity = $capacity;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return count($this->products);
    }

    /**
     * @param Product $product
     *
     * @return void
     * @throws CapacityException
     */
    public function addProduct(Product $product): void
    {

        if ($this->capacity > $this->getQuantity()) {
            $this->products[] = $product;
        } else {
            throw new CapacityException(ErrorMessage::NOT_ENOUGH_CAPACITY);
        }
    }

    /**
     * @return int
     */
    public function getVacancies(): int
    {
        return $this->getCapacity() - $this->getQuantity();
    }

    /**
     * @param array $products
     *
     * @return void
     * @throws CapacityException
     */
    public function addProducts(array $products): void
    {

        if (($this->capacity - $this->getQuantity()) >= count($products)) {
            $this->products = array_merge($this->products, $products);
        } else {
            throw new CapacityException(ErrorMessage::NOT_ENOUGH_CAPACITY);
        }
    }

    /**
     * @param array $productIds
     *
     * @return array
     */
    public function removeProducts(array $productIds): array
    {

        $this->products = array_filter($this->products, function (Product $product) use
        (
            &
            $productIds
        ) {
            $inStore = !in_array($product->getId(), $productIds, true);
            if (!$inStore) {
                $key = array_search($product->getId(), $productIds, true);
                unset($productIds[$key]);
            }
            return $inStore;
        });

        return $productIds;
    }

    /**
     * @param int $productId
     *
     * @return bool
     */
    public function containProduct(int $productId): bool
    {
        $productIds = array_map(function (Product $product) {
            return $product->getId();
        }, $this->products);

        return in_array($productId, $productIds, true);
    }
}