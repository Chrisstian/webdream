<?php

namespace Enum;

/**
 * Enum BrandEnum
 */
enum BrandEnum: int
{

    case BAD = 1;
    case WEAK = 2;
    case MIDDLE = 3;
    case GOOD = 4;
    case EXCELENT = 5;

    /**
     * @param int $quality
     *
     * @return BrandEnum|null
     */
    public static function getBrandQuality(int $quality): ?BrandEnum
    {
        $cases = self::cases();
        foreach ($cases as $case) {
            if ($case->value === $quality) {
                return $case;
            }
        }

        return null;
    }
}
